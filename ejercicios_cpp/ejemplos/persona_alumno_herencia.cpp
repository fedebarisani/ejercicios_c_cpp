#include <iostream>
#include <stdlib.h>
#include <list>
#include <vector>

using namespace std;

/////////////////////////////////////////////////////////////////////////////////

class Persona
{
    private:    //Atributos
        string nombre;
        int edad;
    public:     //Metodos
        Persona(string, int);   //Constructor
        void mostrarPersona();
};

class Alumno : public Persona
{
    private:    //Atributos
        string legajoAlumno;
        float nota1;
        float nota2;
        float nota3;
        string condicion;
    public:     //Metodos
        Alumno(string, int, string, float, float, float); //Constructor
        void mostrarAlumno();
};

/////////////////////////////////////////////////////////////////////////////////

Persona::Persona(string nombre_in, int edad_in)
{
    nombre = nombre_in;
    edad = edad_in;
}

void Persona::mostrarPersona()
{
    cout << "-------------------------------" << endl;
    cout << "Nombre: " << nombre << endl;
    cout << "Edad: " << edad << endl;
}

Alumno::Alumno(string nombre_in, int edad_in, string legajoAlumno_in, float nota1_in, float nota2_in, float nota3_in) : Persona (nombre_in, edad_in)
{
    legajoAlumno = legajoAlumno_in;
    nota1 = nota1_in;
    nota2 = nota2_in;
    nota3 = nota3_in;
    if(nota1 >= 8 && nota2 >= 8 && nota3 >= 8)
    {
        condicion = "Promocionado";
    }
    else if(nota1 >= 6 && nota2 >= 6 && nota3 >= 6)
    {
        condicion = "Regular";
    }
    else
    {
        condicion = "Libre";
    }
}

void Alumno::mostrarAlumno()
{
    mostrarPersona();
    cout << "Legajo alumno: " << legajoAlumno << endl;
    cout << "1er Nota: " << nota1 << endl;
    cout << "2da Nota: " << nota2 << endl;
    cout << "3er Nota: " << nota3 << endl;
    cout << "Condicion: " << condicion << endl;
}

/////////////////////////////////////////////////////////////////////////////////

int main ()
{
    int i = 0;
    string nombre;
    int edad;
    string legajoAlumno;
    float nota1;
    float nota2;
    float nota3;

    //while(1)
    //{
        cout << "Nombre: ";
        cin >> nombre;
        cout << "Edad: ";
        cin >> edad;
        cout << "Legajo: ";
        cin >> legajoAlumno;
        cout << "1er Nota: ";
        cin >> nota1;    
        cout << "2da Nota: ";
        cin >> nota2;    
        cout << "3er Nota: ";
        cin >> nota3;            

        Alumno alumno1 (nombre, edad, legajoAlumno, nota1, nota2, nota3);
        //vector <Alumno> vector_alumnos (i, alumno1);
        alumno1.mostrarAlumno();
        //i++;

        //Alumno alumno1 ("Javier", 40, "2323232", 8.5, 7.5, 6.4);
        //vector <Alumno> vector_alumnos (i, alumno1);
        //alumno1.mostrarAlumno();

        for (i=0; i<2; i++)
        {

        }

        
    //}
    return 0;
}
