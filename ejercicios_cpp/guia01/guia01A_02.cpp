/*
Ejercicio 2
Escriba un programa que pida una palabra e imprima el siguiente patrón:
        f
       e e
      d   d
     e     e
    r       r
   i         i
  c           c
 o             o
 ociredefederico
*/

#include <stdint.h>
#include <string>
#include <iostream>
#include <time.h>

using namespace std;

int main()
{
    int i, j;
    int n=0;
    int len;
    string word;
    char letter;
    string::iterator it;

    cout << "Ingrese una palabra: ";
    cin >> word;
    
    len = word.length();

    //Piramide
    for (it=word.begin(), i=1; it!=word.end(), i<=word.length(); it++,i++)
    {
        for(j=len-i; j>=0; j--)
            cout << " ";
        cout << *it;

        if (i > 1)
        {
            for(j=1; j<i+n; j++)
                cout << " ";      
            cout << *it << endl;    
            n++;        
        }
        else
            cout << endl;
    }
    cout << " ";

    //Invertida
    for (it=word.end(); it!=word.begin(); it--)
        cout << *it; 
    
    //No invertida
    for (it=word.begin(); it!=word.end(); it++)
        cout << *it; 
       
    cout << endl;

    return 0;
}