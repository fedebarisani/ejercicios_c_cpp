/*
Ejercicio 1
Crear una clase llamada empleado que contenga como miembro dato (atributos) el nombre
y el número de identificación de empleado, y como funciones miembros (métodos) 
ingresarDatos() y mostrarDatos() que lean datos desde el teclado y que visualicen en 
pantalla estos datos respectivamente. Escribir un programa que utilice la clase, 
creando un array de tipo empleado y luego llenándolo con datos correspondientes a 5 
empleados. Una vez llenado el array, visualizar los datos de todos los empleados.
*/

#include <string>
#include <iostream>

using namespace std;

/////////////////////////////////////////////////////////////////////////////////

class Empleado
{
    public:
        string nombre;
        int id;
        
        void ingresarDatos();
        void mostrarDatos();
};

void Empleado::ingresarDatos()
{
    cout << "Nombre: ";
    cin >> nombre;
    cout << "ID: ";
    cin >> id;
}

void Empleado::mostrarDatos()
{
    cout << "ID: " << id << "\tNombre: " << nombre << endl;
}

/////////////////////////////////////////////////////////////////////////////////

int main()
{
    Empleado emp[5];
    int i;

    for(i=0; i<5; i++)
    {
        emp[i].ingresarDatos();
    }
    for(i=0; i<5; i++)
    {
        emp[i].mostrarDatos();
    }    
}
