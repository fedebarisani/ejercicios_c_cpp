#include <string>
#include <iostream>
#include <cmath>
#include <list>

using namespace std;

/////////////////////////////////////////////////////////////////////////////////

class Data
{
    protected:
        bool i = 0;
        float temp;
        float temp_sum;
        float temp_min;
        float temp_max;
        float hum;
        float pre;
    public:   
        //Constructor por defecto
        Data()
        {
            temp = 0;
            hum = 0;
            pre = 0;
            temp_max = 0;
            temp_min = 0;
            temp_sum = 0;
        }
        //Constructor parametrizado
        Data (float temp_in, float hum_in, float pre_in)
        {
            temp = temp_in;
            hum = hum_in;
            pre = pre_in;
        }

        //Sets
        void setTemp(float temp_in)
        {
            temp_sum = temp_sum + temp_in;
            if (i == 0)
            {
                i = 1;
                temp_max = temp_in;
                temp_min = temp_in;
            }
            else if(temp_in > temp)
            {
                temp_max = temp_in;
            }
            else if (temp_in < temp)
            {
                temp_min = temp_in;
            }
            temp = temp_in;
        }
        void setHum(float hum_in)
        {
            hum = hum_in;
        }         
        void setPre(float pre_in)
        {
            pre = pre_in;
        }

        //Gets
        float getTemp()
        {
            return temp;
        }                       
        float getHum()
        {
            return hum;
        }
        float getPre()
        {
            return pre;
        }  
};

/////////////////////////////////////////////////////////////////////////////////

class DataOutput : public Data
{
    public:
        void print(int i)
        {
            system("clear");
            cout << "------------------------------------------------------------------------------" << endl;
            cout << "La temperatura actual es de "<< temp << "°C, y la humedad es del " << hum << "%." << endl;
            cout << "------------------------------------------------------------------------------" << endl;
            cout << "Avg\tMin\tMax" << endl;
            cout << temp_sum/i << "\t" << temp_min << "\t" << temp_max << endl;
            cout << "------------------------------------------------------------------------------" << endl;            
            if(temp >= 32)
            {
                cout << "Día de mucho calor." << endl;
            }
            if(temp >= 27 && temp < 32)
            {
                cout << "Día de calor." << endl;
            }
            else if (temp >= 20 && temp < 27)
            {
                cout << "Día agradable." << endl;
            }
            else if (temp < 20 && temp > 15)
            {
                cout << "Día destemplado." << endl;
            }            
            else if (temp <= 15 && temp >= 5)
            {
                cout << "Día frío." << endl;
            }             
            else if (temp < 5)
            {
                cout << "Día muy frio." << endl;
            }
            cout << "------------------------------------------------------------------------------" << endl;            
        }
};

/////////////////////////////////////////////////////////////////////////////////

int main()
{
    float temp_in;
    float hum_in;
    float pre_in;
    bool loop = 1;
    int opcion;
    int i = 0;

    list <DataOutput> lista;

    DataOutput *data_out = new DataOutput;

    while (loop)
    {
        system("clear");
        cout << "------------------------------------------------------------------------------" << endl;
        cout << "Ingresar temperatura (°C): ";
        cin >> temp_in;
        cout << "Ingresar humedad (%): ";
        cin >> hum_in;
        cout << "Insert pressure (Atm): ";
        cin >> pre_in;        

        data_out->setTemp(temp_in);
        data_out->setHum(hum_in);
        data_out->setPre(pre_in);

        lista.push_back(*data_out);

        i = lista.size();

        data_out->print(i);
        
        cout << "\nPresione:\n1. Para ingresar un nuevo dato." << endl;
        cin >> opcion;
        if (opcion == 1)
            loop = 1;
        else
            loop = 0;
    }
}
