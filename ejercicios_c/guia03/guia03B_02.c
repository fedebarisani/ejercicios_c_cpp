/*
Ejercicio 2
Escriba un programa que sea capaz de contar las palabras en un archivo de texto plano mediante el uso
de funciones de entrada/salida de archivos (file.h) y de la biblioteca de manejo de cadenas de caracteres
(string.h). Compruebe el funcionamiento con un archivo trivial de dos palabras (Hola Mundo!) y luego 
utilice el archivo lorem_ipsum.txt suministrado por la materia.
*/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int main (void)
{
    FILE *f;

    int i=0;
    char buffer [100];
    char *ptrToken; 

    f = fopen("file_text.txt", "r");

	if(f == NULL)
	{
	    printf("ERROR, no se pudo abrir el archivo\n");
		return 0;
	}

    fread(buffer, 100, 1, f);

    ptrToken = strtok(buffer, " ");

    while (ptrToken != NULL)
    {
        //printf( "%s\n", ptrToken ); 
        ptrToken = strtok(NULL, " "); 
        i++;
    }
    printf("Cantidad de palabras en el archivo: %d\n", i);
}


