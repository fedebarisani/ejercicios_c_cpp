/*
Ejercicio 2
Escribir un programa interactivo que acepte la siguiente información sobre equipos de la liga de fútbol, para el torneo actual:
- Nombre del equipo.
- Número de victorias.
- Número de derrotas.
- Número de empates.
- Cantidad de goles.
Introducir esa información para todos los equipos deseados. Permitir al usuario ver el listado ordenado de equipos según:
- Victorias
- Derrotas
- Cantidad de goles.
La información para cada equipo estará almacenada en un arreglo almacenado dinámicamente en memoria. La/s función/es de ordenamiento 
utilizaran almacenamiento dinámico el cual será liberado luego de retornar la información solicitada.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

struct club
{
	char nombre [15];
	int pg;
	int pp;
	int pe;
    int g;
	struct club *next;
};

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void agregarClub (struct club **p, char *nombre, int pg, int pp, int pe, int g)
{
	struct club *ant;
	struct club *act;
	struct club *nuevo;
	
	ant = act = *p;
	
	nuevo = (struct club*) malloc (sizeof (struct club));
	
	if (nuevo == NULL)
	{
		system ("clear");
		printf ("ERROR. No hay memoria disponible.\n");
		return;
	}
	
	if (*p == NULL) //lista vacia
	{
		*p = nuevo;
		nuevo->next = NULL;
		strcpy (nuevo->nombre, nombre);
		nuevo->pg = pg;
		nuevo->pp = pp;
		nuevo->pe = pe;
		nuevo->g = g;
		return;
	}
	while (act != NULL)
	{
		if (act->pg < pg) //el de mas pg va primero
		{
			break;
		}
		ant = act;
		act = act->next;
	}
	if (ant == act) //al principio
	{
		*p = nuevo;
		nuevo->next = act;
		strcpy (nuevo->nombre, nombre);
		nuevo->pg = pg;
		nuevo->pp = pp;
		nuevo->pe = pe;
		nuevo->g = g;	
		return;
	}
	if (act == NULL) //al final
	{
		ant->next = nuevo;
		nuevo->next = NULL;
		strcpy (nuevo->nombre, nombre);
		nuevo->pg = pg;
		nuevo->pp = pp;
		nuevo->pe = pe;
		nuevo->g = g;
		return;
	}
	ant->next = nuevo; //en el medio
	nuevo->next = act;
	strcpy (nuevo->nombre, nombre);
	nuevo->pg = pg;
	nuevo->pp = pp;
	nuevo->pe = pe;
	nuevo->g = g;
	return;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void buscarClub (struct club **p, char *nombre)
{
	struct club *act;
	struct club *ant;
	int opcion;
	
	ant = act = *p;
	
	while (act != NULL)
	{
		if (strcmp(act->nombre, nombre) == 0)
		{
			system ("clear");
			printf ("BUSCAR CLUB\n");
			printf ("----------------------------------------------------\n");
			printf ("Club: %s\n", act->nombre);
			printf ("Partidos ganados (PG): %d\n", act->pg);
			printf ("Partidos perdidos (PP): %d\n", act->pp);
			printf ("Partidos empatados (PE): %d\n", act->pe);
			printf ("Goles (G): %d\n", act->g);
			
			printf ("Presione cualquier numero para volver al menu\n");
			scanf ("%d", &opcion);
			if (opcion != -239517)
			{
				return;
			}
		}
		ant = act;
		act = act->next;
	}
	system ("clear");
	printf ("Club no encontrado.\n");
	printf ("Presione cualquier numero para volver al menu\n");
	scanf ("%d", &opcion);
	if (opcion != -239517)
	{
		return;
	}	
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void borrarClub (struct club **p, char *nombre)
{
	struct club *act;
	struct club *ant;
	int opcion;
	
	ant = act = *p;
	
	while (act != NULL)
	{
		if (strcmp(act->nombre, nombre) == 0)
		{
			if (ant == act)
			{
				*p = act->next;
				free (ant);
				return;
			}
			if (act->next == NULL)
			{
				ant->next = NULL;
				free (act);
				return;
			}
			ant->next = act->next;
			free (act);
			return;
		}
		ant = act;
		act = act->next;
	}
	system ("clear");
	printf ("Club no encontrado.\n");
	printf ("Presione cualquier numero para volver al menu\n");
	scanf ("%d", &opcion);
	if (opcion != -239517)
	{
		return;
	}	
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void imprimirClubes (struct club *p, int orden)
{
	struct club *act;
	struct club *ant;
	struct club *aux;

	int opcion;
	
	ant = act = aux = p;
	
	if (orden == 1)	//ordeno por PG
	{
		while(act != NULL)
		{
			if(ant->pg < act->pg)
			{
				aux->pg = ant->pg;
				ant->pg = act->pg;
				act->pg = aux->pg;
			}
			ant = act;
			act = act->next;		
		}
	}
	if (orden == 2)	//ordeno por PP
	{
		while(act != NULL)
		{
			if(ant->pp < act->pp)
			{
				aux->pp = ant->pp;
				ant->pp = act->pp;
				act->pp = aux->pp;
			}
			ant = act;
			act = act->next;		
		}
	}
	if (orden == 3)	//ordeno por G
	{
		while(act != NULL)
		{
			if(ant->g < act->g)
			{
				aux->g = ant->g;
				ant->g = act->g;
				act->g = aux->g;
			}
			ant = act;
			act = act->next;		
		}
	}		

	ant = act = p;

	system ("clear");
	printf ("LISTADO DE CLUBES\n");
	printf ("----------------------------------------------------\n");
	printf ("Club\t\tPG\tPP\tPE\tG\n");
	
	while (act != NULL)
	{
		printf ("----------------------------------------------------\n");
		printf ("--> %s\t%d\t%d\t%d\t%d\n", act->nombre, act->pg, act->pp, act->pe, act->g);		
		ant = act;
		act = act->next;
	}
	printf ("Presione cualquier numero para volver al menu\n");
	scanf ("%d", &opcion);
	if (opcion != -239517)
	{
		return;
	}	
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void borrarNodos (struct club **p)
{
	struct club *act;
	struct club *ant;
	
	ant = act = *p;
	
	while (act != NULL)
	{
		ant = act;
		act = act->next;
		free (act);
	}
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

int main (void)
{
	struct club *inicio = NULL;

	int opcionmenu = 0;
	char nombre [15];
	int pg;
	int pp;
	int pe;
	int g;

	while (1)
	{
		switch(opcionmenu)
		{
			case 0:
				system ("clear");
				printf ("MENU\n");
				printf ("----------------------------------------------------\n");
				printf ("Presione:\n");
				printf ("1. Para ingresar un club.\n");
				printf ("2. Para buscar un club.\n");
				printf ("3. Para borrar un club.\n");
				printf ("4. Para listar los clubes ordenados por PG.\n");
				printf ("5. Para listar los clubes ordenados por PP.\n");
				printf ("6. Para listar los clubes ordenados por G.\n");
				printf ("99. Para salir del programa.\n");
				scanf ("%d", &opcionmenu);
			break;

			case 1:
				system ("clear");
				printf ("INGRESAR CLUB\n");
				printf ("----------------------------------------------------\n");
				printf ("Ingrese los siguientes datos del club:\n");
				printf ("Nombre del club: ");
				scanf ("%s", nombre);
				printf ("Partidos ganados (PG): ");
				scanf ("%d", &pg);
				printf ("Partidos perdidos (PP): ");
				scanf ("%d", &pp);
				printf ("Partidos empatados (PE): ");
				scanf ("%d", &pe);
				printf ("Goles (G): ");
				scanf ("%d", &g);
				
				agregarClub (&inicio, nombre, pg, pp, pe, g);
				
				opcionmenu = 0;			
			break;

			case 2:
				system ("clear");
				printf ("BUSCAR CLUB\n");
				printf ("----------------------------------------------------\n");
				printf ("Ingrese el nobmre del club que quiere buscar: ");
				scanf ("%s", nombre);
				
				buscarClub (&inicio, nombre);
				
				opcionmenu = 0;
			break;

			case 3: 
				system ("clear");
				printf ("BORRAR CLUB\n");
				printf ("----------------------------------------------------\n");
				printf ("Ingrese el nombre del club que quiere borrar: ");
				scanf ("%s", nombre);
				
				borrarClub (&inicio, nombre);
				opcionmenu = 0;
			break;			

			case 4:
				imprimirClubes (inicio, 1);
				opcionmenu = 0;
			break;

			case 5:
				imprimirClubes (inicio, 2);
				opcionmenu = 0;			
			break;

			case 6:
				imprimirClubes (inicio, 3);
				opcionmenu = 0;					
			break;

			case 99:
				return 0;
			break;

			default:
				opcionmenu = 0;
			break;
		}
	}
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
