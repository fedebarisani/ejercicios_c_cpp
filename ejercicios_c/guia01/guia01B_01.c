/*
Ejercicio 1
Escribir un bucle for o while que lea los caracteres de una cadena de caracteres llamada texto
y los escriba en sentido opuesto en otra cadena llamada inverso. Suponga que la cadena texto
contendrá 80 caracteres como máximo, pero la longitud del texto será determinado en tiempo de
ejecución, dependiendo de la cadena que ingrese el usuario. Utilizar el operador coma dentro
del bucle for (while). Utilizar las funciones scanf (o fgets) y printf para obtener y mostrar
los datos respectivamente. El programa finalizará solamente cuando el usuario ingrese “-1” por
teclado.
*/

#include <stdio.h>

void tools_invert_string (char *out, size_t len, char *in)
{
    int i, j;
    int end;

    for(i=0; i<len; i++)
    {
        if (in[i] == '\0')
        {
            end = i;
            break;
        }
    }
    out[end] = '\0';
    j = end - 1;
    for (i=0; i<end; i++)
    {
        out[j] = in[i];
        j--;
    }
    //Debug
    printf("%s\n", out);
}

void main (void)
{
    char string_in[80];
    char string_out[80];
    int len;

    while(1)
    {
        printf("Ingrese la cadena de caracteres que desea invertir: ");
        len = sizeof(string_in);
        fgets(string_in, len, stdin);
        
        if(string_in[0] == '-' && string_in[1] == '1')
        {
            break;
        }
        
        tools_invert_string(string_out, len, string_in);
    }
}
