/*
Ejercicio 1
Un programa en C contiene las siguientes instrucciones:
#include <stdio.h>
int i = 102;
int j = -56;
long ix = -158693157400;
unsigned u = 35460;
float x = 12.687;
double dx = 0.000000025;
char c = ‘C’;

Escribir una función que utilice printf para cada una de las siguientes situaciones, suponiendo que todos los enteros se desean presentar como cantidades decimales.
A. Escribir los valores de i, j, x y dx suponiendo que cada cantidad entera tiene una longitud de campo mínima de cuatro caracteres y cada cantidad en coma flotante se presenta en notación exponencial con un total de al menos 14 caracteres y no más de 8 cifras decimales.
B. Repetir A, visualizando cada cantidad en una línea.
C. Escribir los valores de i, ix, j, x y u suponiendo que cada cantidad entera tendrá una longitud de campo mínima de cinco caracteres, el entero largo tendrá una longitud de campo mínima de 12 caracteres y la cantidad en coma flotante tiene al menos 10 caracteres con un máximo de cinco cifras decimales. No incluir el exponente.
D. Repetir C, visualizando las tres primeras cantidades en una línea, seguidas de una línea en blanco y las otras dos cantidades en la línea siguiente.
E. Escribir los valores de i, u y c, con una longitud de campo mínima de seis caracteres para cada cantidad entera. Separar con tres espacios en blanco cada cantidad.
F. Escribir los valores de j, u y x. Visualizar las tres cantidades enteras con una longitud de campo mínima de cinco caracteres. Presentar la cantidad en coma flotante utilizando la conversión tipo f, con una longitud mínima de 11 caracteres y un máximo de cuatro cifras decimales.
G. Repetir F, con cada dato ajustado a la izquierda dentro de su campo.
H. Repetir F, apareciendo un signo (tanto + como -) delante de cada dato con signo.
I. Repetir F, rellenando el campo de cada entidad entera con ceros.
J. Repetir F, apareciendo el valor de x con un punto decimal.
*/

#include <stdio.h>

int i = 102;
int j = -56;
long ix = -158693157400;
unsigned u = 35460;
float x = 12.687;
double dx = 0.000000025;
char c = 'C';


void funcion_a (void)
{
    printf("%.4d %.4d %14.8f %14.8lf\n", i, j, x, dx);
}

void funcion_c (void)
{
    printf("%.5d %.5ld %.5d\n%12.10f %.5d\n", i, ix, j, x, u);
}

void funcion_e (void)
{
    printf("%.6d   %.6d   %d\n", i, u, c);
}

void funcion_f (void)
{
    printf("%5d   %5d   %4.11f\n", j, u, x);
}

void funcion_g (void)
{
    printf("%-5d   \n%-5d   \n%-4.11f\n", j, u, x);
}

void funcion_h (void)
{
    printf("%+-5d   \n%+-5d   \n%+-4.11f\n", j, u, x);
}

void funcion_i (void)
{
    printf("%+-.5d   \n%+-.5d   \n%+-4.11f\n", j, u, x);
}

void funcion_j (void)
{
    printf("%+-.5d   \n%+-.5d   \n%+-4.1f\n", j, u, x);
}

void main (void)
{
    funcion_a();    //item b

    funcion_c();    //item d

    funcion_e();    //item e

    funcion_f();    //item f

    funcion_g();    //item g

    funcion_h();    //item h
    
    funcion_i();    //item i

    funcion_j();    //item j
}

