/*
Ejercicio 3
Escribir un programa que simule un juego de blackjack entre dos jugadores. La computadora no será un 
participante en el juego, simplemente dará las cartas a cada jugador y proveerá a cada jugador con una
o más cartas adicionales cuando este la solicite.
Las cartas se dan en orden, primero una carta a cada jugador, después otra carta a cada uno. Se pueden 
demandar cartas adicionales.
El objeto del juego es obtener 21 puntos, o tantos puntos como sea posible sin exceder de 21 en cada mano.
Un jugador es automáticamente descalificado si las cartas en su mano exceden de 21 puntos. Las figuras 
cuentan 10 puntos y un as puede contar como 1 u 11 puntos. Así, un jugador puede obtener 21 puntos si tiene 
un as y una figura o un 10. Si el jugador tiene menos puntos con sus dos primeras cartas, puede pedir una 
carta o más, mientras su puntuación no pase de 21.
Utilizar números aleatorios para simular el reparto de las cartas. Asegurar la inclusión de una condición
para que la misma carta no sea dada más de una vez.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>


int tools_rand_int(int array[], int len)
{
    int num;

    srand(time(0));
    num = (rand() % (len));

    return array[num];
}


int main (void)
{
    int estado = 0;
    int jugador[2] = {1, 2};
    int cuenta[2] = {0, 0};
    int stop[2] = {0, 0};
    int i = 0;
    int aux;
    int cartas[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13};
    int len = sizeof(cartas)/sizeof(cartas[0]);
    int cant_cartas[] = {4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4};

    system("clear");

    while(1)
    {
        switch (estado)
        {
            case 0: 
                if (stop[0] == 1 && stop[1] == 1)
                {
                    printf("\nFin del juego.\n");
                    return 0;
                }
                else if (stop[i] != 1)
                {
                    printf("\n-------------------------------------\n");
                    printf("BLACKJACK\n");
                    printf("-------------------------------------\n");    
                    printf("Jugador1: %d\t\tJugador2: %d\n", cuenta[0], cuenta[1]);
                    printf("-------------------------------------\n");                                    
                    printf("Jugador%d su cuenta está en %d. Presione:\n", jugador[i], cuenta[i]);
                    printf("1. Para recibir una carta.\n");
                    printf("2. Para plantarse.");
                    scanf("%d", &estado);   
                }
                else
                {
                    if (i==0)
                        i++;
                    else
                        i--;
                }
            break;
            
            case 1:
                aux = tools_rand_int(cartas, len);
                cant_cartas[aux-1] = cant_cartas[aux-1] - 1;
                if (cant_cartas > 0)
                {
                    if (aux == 1)
                    {
                        printf("Su carta es un %d. Presione:\n", aux);
                        printf("1. Para tomar su carta como un 1.\n");
                        printf("11. Para tomar su carta como un 11.");
                        scanf("%d", &aux);
                        if (aux != 1 || aux != 11)
                        {
                            printf("Mal jugado!\n");
                            estado = 2;
                            break;
                        }
                        //El jugador no puede ingresar otro valor
                    }
                    else if (aux == 10 || aux == 11 || aux == 12 || aux == 13)
                    {
                        aux = 10;              
                    }
                    cuenta[i] = cuenta[i] + aux;
                    printf("Su carta es un %d. ", aux);
                    printf("Su cuenta esta en %d.\n", cuenta[i]);
                    if (cuenta[i] == 21)
                    {
                        printf("\nBlackjack! Jugador%d gana el juego.\n", jugador[i]);
                        stop[i] = 1;    
                    }
                    else if (cuenta[i] > 21)
                    {
                        printf("\nMayor que 21! Jugador%d pierde el juego.\n", jugador[i]);
                        stop[i] = 1;                      
                    }
                    if (i==0)
                        i++;
                    else
                        i--;
                    estado = 0;
                }
                else
                {
                    estado = 1;
                }
            break;

            case 2:
                printf("Jugador%d se plantó con %d.\n", jugador[i], cuenta[i]);
                stop[i] = 1;
                if (i==0)
                    i++;
                else
                    i--;
                estado = 0;                
            break;

            default:
            break;
        }    
    }
}